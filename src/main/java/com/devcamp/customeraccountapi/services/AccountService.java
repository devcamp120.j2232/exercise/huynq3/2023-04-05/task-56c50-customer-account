package com.devcamp.customeraccountapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customeraccountapi.models.Account;
@Service
public class AccountService extends CustomerService{
    Account account1 = new Account(201, customer1, 10000);
    Account account2 = new Account(202, customer2, 20000);
    Account account3 = new Account(203, customer3, 30000);
    public ArrayList<Account> getAllAccounts(){
        ArrayList<Account> accountList = new ArrayList<>();
        accountList.add(account1);
        accountList.add(account2);
        accountList.add(account3);
        return accountList;

    }
}
