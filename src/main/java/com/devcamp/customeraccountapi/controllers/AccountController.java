package com.devcamp.customeraccountapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customeraccountapi.models.Account;
import com.devcamp.customeraccountapi.services.AccountService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class AccountController {
    @Autowired
    AccountService accountService;
    @GetMapping("/accounts")
    public ArrayList<Account> getAllAccountsApi(){
        return accountService.getAllAccounts();
    }
}
